package org.app;


import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;

public class Sorter {

    /**
     * The main method which is the entry point for the application.
     * It takes an array of strings as arguments, converts them to integers and sorts them.
     * If no arguments are provided, it throws an IllegalArgumentException.
     * If more than 10 arguments are provided, it throws an IllegalArgumentException.
     * If any of the arguments cannot be converted to an integer, it throws a NumberFormatException.
     *
     * @param args the command line arguments
     * @throws IllegalArgumentException if no arguments are provided or if more than 10 arguments are provided
     * @throws NumberFormatException    if any of the arguments cannot be converted to an integer
     */
    public static void main(String[] args) {

        if (ArrayUtils.isEmpty(args)) {
            throw new IllegalArgumentException("No input provided");
        }
        if (args.length > 10) {
            throw new IllegalArgumentException("Limit number exceeded");
        }

        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                throw new NumberFormatException("Invalid input: " + e.getMessage());
            }
        }
        Arrays.sort(numbers);
        System.out.println(ArrayUtils.toString(numbers));
        System.setProperty("sortedArray", Arrays.toString(numbers));
    }
}
