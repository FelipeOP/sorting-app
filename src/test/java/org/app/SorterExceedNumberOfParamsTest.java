package org.app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class SorterExceedNumberOfParamsTest {

    private final String[] args;

    public SorterExceedNumberOfParamsTest(String args) {
        this.args = args.split(" ");
    }


    @Parameterized.Parameters()
    public static Collection<String> data() {
        return Arrays.asList(
                "1 2 3 4 5 6 7 8 9 10 11",
                "1 2 3 4 5 6 7 8 9 10 11 12",
                "1 2 3 4 5 6 7 8 9 10 11 12 13"
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSorterWhenMaxArgsExceededThenThrowIllegalArgumentException() {
        Sorter.main(args);
    }

}



