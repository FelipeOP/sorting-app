package org.app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SorterInvalidParamsTest {

    private final String[] args;

    public SorterInvalidParamsTest(String args) {
        this.args = args.split(" ");
    }

    @Parameterized.Parameters()
    public static Collection<String> data() {
        return Arrays.asList(
                "1 2 3 NaN",
                "1, 2, 3",
                "1 2 q 4 5",
                " 1 2 3 4 5"
        );
    }

    @Test(expected = NumberFormatException.class)
    public void testSorterWhenNoArgsThenThrowIllegalArgumentException() {
        Sorter.main(args);
    }


}
