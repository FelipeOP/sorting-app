package org.app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SorterNoParamsTest {

    private final String[] args;

    public SorterNoParamsTest(String args) {
        this.args = args.split(" ");
    }

    @Parameterized.Parameters()
    public static Collection<String> data() {
        return Arrays.asList("", " ", "    ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSorterWhenNoArgsThenThrowIllegalArgumentException() {
        Sorter.main(args);
    }


}
