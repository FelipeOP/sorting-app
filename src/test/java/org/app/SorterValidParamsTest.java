package org.app;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SorterValidParamsTest {

    private final String[] args;
    private final String expected;

    public SorterValidParamsTest(String args, String expected) {
        this.args = args.split(" ");
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Collection<Object[]> data() {
        return Arrays.asList(
                new Object[]{"1", "[1]"},
                new Object[]{"2 1", "[1, 2]"},
                new Object[]{"2 1 3", "[1, 2, 3]"},
                new Object[]{"2 1 3 4", "[1, 2, 3, 4]"},
                new Object[]{"3 1 5 2 4", "[1, 2, 3, 4, 5]"},
                new Object[]{"3 1 5 2 4 6", "[1, 2, 3, 4, 5, 6]"},
                new Object[]{"3 1 5 2 4 6 7", "[1, 2, 3, 4, 5, 6, 7]"},
                new Object[]{"3 1 5 2 4 6 7 8", "[1, 2, 3, 4, 5, 6, 7, 8]"},
                new Object[]{"3 1 5 2 4 6 7 8 9", "[1, 2, 3, 4, 5, 6, 7, 8, 9]"},
                new Object[]{"3 1 5 2 4 6 7 8 9 10", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"}
        );
    }

    @Test
    public void testSorterWhenNoArgsThenThrowIllegalArgumentException() {
        Sorter.main(args);
        String sortedArray = System.getProperty("sortedArray");
        Assert.assertEquals(expected, sortedArray);
    }


}
